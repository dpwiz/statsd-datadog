import Network.StatsD
import qualified Data.Text as T

main :: IO ()
main = do
    sd <- connectStatsD "localhost" "81250"
    sendStatsDIO sd (event "title" "text")
        { eSourceType = Just "console"
        }
