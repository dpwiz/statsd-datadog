-- | Datadog extension to statsd protocol for custom events to appear in your HQ.

module Network.StatsD.Event
    ( Event(..), event
    ) where

import Network.StatsD.Datagram
import Network.StatsD.Tags

import           Data.Monoid
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Text.Lazy.Builder as TB

-- | Set event fields for fancy effects in event log.
data Event = Event
    { eTitle      :: Text
    , eText       :: Text
    , eDate       :: Maybe Text
    , eHostname   :: Maybe Text
    , eAggrKey    :: Maybe Text
    , ePriority   :: Maybe Text
    , eSourceType :: Maybe Text
    , eAlertType  :: Maybe Text
    , eTags       :: [(Text, Text)]
    } deriving (Show)

-- | Construct a basic event.
event :: Text -> Text -> Event
event title text = Event
    { eTitle      = title
    , eText       = text
    , eDate       = Nothing
    , eHostname   = Nothing
    , eAggrKey    = Nothing
    , ePriority   = Nothing
    , eSourceType = Nothing
    , eAlertType  = Nothing
    , eTags       = []
    }

instance ToDatagram Event where
    toDatagram (Event{..}) =
        let header =
                [ TB.fromText "_e{"
                , TB.fromString . show $ T.length eTitle
                , TB.singleton ','
                , TB.fromString . show $ T.length eText
                , TB.fromText "}:"
                , TB.fromText eTitle
                , TB.singleton '|'
                , TB.fromText eText
                ]

        in Datagram $ mconcat
             [ mconcat header
             , mprefixed 'd' eDate
             , mprefixed 'h' eHostname
             , mprefixed 'k' eAggrKey
             , mprefixed 'p' ePriority
             , mprefixed 's' eSourceType
             , mprefixed 't' eAlertType
             , tags eTags
             ]

instance Tagged Event where
    getTags = eTags
    setTags e ts = e { eTags = ts }
