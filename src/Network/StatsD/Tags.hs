module Network.StatsD.Tags
    ( Tags
    , tagged
    , Tagged(..)
    , ToTag(..)
    ) where

import Data.Text (Text)
import Data.Monoid

type Tags = [(Text, Text)]

class Tagged a where
    getTags :: a -> Tags
    setTags :: a -> Tags -> a

class ToTag a where
    toTag :: a -> (Text, Text)

instance ToTag (Text, Text) where
    toTag = id

instance ToTag Text where
    toTag t = (t, "")

-- | Add tags (with or without values). Due to OverloadedStrings instances interference you have to pin types using `Data.Text.pack` or @::@.
--  
-- > counter_ "pings" `tagged` [ T.pack "success", "icmp" :: Text, "default" ]
-- >                  `tagged` [ (T.pack "valued", "42" :: Text), ("it", "works") ]
tagged :: (Tagged a, ToTag t) => a -> [t] -> a
tagged a ts = setTags a (getTags a <> map toTag ts)
